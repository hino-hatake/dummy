package hino;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

/**
 * Please be noted that we are using JUnit 4 based Runner which runs tests on the JUnit Platform in a JUnit 4 environment.
 * <p style="font-size:100px">&#128531;</p>
 */
@RunWith(JUnitPlatform.class)
@IncludeClassNamePatterns
@SelectClasses(ModelMapperTest.class)
@IncludeTags("lol")
public class LolSuite {

}
