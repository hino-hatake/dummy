package hino;

import java.util.ArrayList;
import java.util.logging.Logger;
import lombok.extern.slf4j.Slf4j;

/**
 * Hello world!
 */
@Slf4j
public class HelloWorld {

  public static void main(String[] args) {

    System.out.println("Hello World!");

    System.out.println("Classloader of this class: " + HelloWorld.class.getClassLoader());

    System.out.println("Classloader of java.util.logging.Logger: " + Logger.class.getClassLoader());

    System.out.println("Classloader of org.slf4j.Logger at @Slf4j: " + log.getClass().getClassLoader());
    log.info("Hell yeah we can use the @Slf4j!!");

    System.out.println("Classloader of ArrayList: " + ArrayList.class.getClassLoader());

  }
}
