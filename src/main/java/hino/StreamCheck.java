package hino;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;

public class StreamCheck {

  public static void main(String[] args) {
    List<String> strings = Arrays.asList("1", "2", "3");
    List<String> res = strings.stream().filter(s -> s.equals("3")).collect(toList());
    System.out.println(res);
    System.out.println(res.size());
  }
}
