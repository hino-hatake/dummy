package hino;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
/*
  Let's see how Lombok handle with auto-timestamp field
 */
public class LombokBuilderCheck {

  public static void main(String[] args) {

    // this should print test instance with a valid receivedDate
    log.info("TestWithFinalDate data: {}", TestWithFinalDate.builder().test("test").build());

    // this should print test instance with null receivedDate
    log.info("TestWithDate data: {}", TestWithDate.builder().test("test").build());

    // this should print test instance with a valid receivedDate
    log.info("TestWithDate data: {}", new TestWithDate());
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  private static class TestWithFinalDate {

    private String test;

    @Builder.Default
    private final Date receivedDate = new Date();
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  private static class TestWithDate {

    private String test;

    private Date receivedDate = new Date();
  }
}
