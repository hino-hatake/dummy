package hino;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Read properties file from outside jar file
 */
public class PropertiesReader {

    public static final String CONFIG_PROPERTIES_FILE = "config.properties";

    public static final String CONFIG_PROPERTIES_PATH = System.getProperty("user.dir") + "\\" + CONFIG_PROPERTIES_FILE;

    public static void main(String[] args) throws IOException {
        Properties prop = new Properties();
        System.out.println("External config file: " + CONFIG_PROPERTIES_PATH);

        prop.load(new FileInputStream(CONFIG_PROPERTIES_PATH));
        // or in case config file is inside classpath
        // prop.load(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(CONFIG_PROPERTIES_FILE)));

        // print the properties
        prop.forEach((key, value) -> System.out.println("key:" + key + ", value: " + value));
    }
}
